The file "ol.css" comes from the official GitHub repository of
OpenLayers, under tag "v3.19.0". To re-download it:

# curl https://raw.githubusercontent.com/openlayers/ol3/v3.19.0/css/ol.css > ol.css

The file "ol-debug.js" comes from the official distribution of
OpenLayers, that is available for download at:
https://github.com/openlayers/ol3/releases/
